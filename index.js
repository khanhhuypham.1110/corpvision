/*Theme function*/

let switcher = document.getElementsByClassName("switch");
let theme = localStorage.getItem("theme");

if (theme == null) {
    setTheme('light')
} else {
    setTheme(theme);
}

for (let i of switcher) {
    i.addEventListener('click', function () {
        let theme = this.dataset.theme;
        setTheme(theme);
    })
}

function setTheme(theme) {
    if (theme == "light") {
        document.getElementById("switcher-id").href = ""
    } else if (theme == "dark") {
        document.getElementById("switcher-id").href = "./darkTheme.css";
    }
    localStorage.setItem("theme", theme);
}

/*scroll effect*/
var header = document.getElementById('header');
window.onscroll = function () { scrollToChange() };

function scrollToChange() {
    let y = document.documentElement.scrollTop.toFixed();
    if (y > 180) {
        // header.style.backgroundColor = 'black'
        header.className = " nav-fixed";
    }
    if (y <= 180) {
        header.className = header.className.replace(" nav-fixed", "")
    }

};







/*search function*/

var searchButtonEle = document.getElementsByClassName("search-btn");
var searchIconEle = document.getElementsByClassName('search-icon');
var closeButtonEle = document.getElementsByClassName('close-icon');
var searchFormEle = document.getElementsByClassName('search-form');
var carouselEle = document.getElementById('carousel');

searchButtonEle[0].addEventListener('click', function () {
    this.className += " search-active";
    searchIconEle[0].className += " search-icon-active";
    closeButtonEle[0].className += " close-icon-active";
    searchFormEle[0].className += " search-form-active";
    carouselEle.style.display = "none";

})

closeButtonEle[0].addEventListener('click', function () {
    this.className = this.className.replace(" close-icon-active", "");
    searchButtonEle[0].className = searchButtonEle[0].className.replace(" search-active", "");
    searchIconEle[0].className = searchIconEle[0].className.replace(" search-icon-active", "");
    searchFormEle[0].className = searchFormEle[0].className.replace(" search-form-active", "");
    carouselEle.style.display = "block";
})










